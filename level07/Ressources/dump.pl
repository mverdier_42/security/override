#!/usr/bin/perl

use strict;

my $i = 0;

while (my $line = <>) {
	chomp($line);
	my $hex = sprintf("0x%08X", $line);
	if ($i % 4 == 0) {
		printf "\n%03d: $hex", $i;
	}
	else {
		print " $hex";
	}
	$i = $i + 1;
}

print "\n";
