#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/uio.h>

int		main()
{
	FILE	*lvl7;

	lvl7 = popen("/home/users/level07/level07", "w");
	if (!lvl7) {
		printf("popen error\n");
		return (0);
	}
	for (int i = 0; i < 224; i++) {
		fprintf(lvl7, "read\n");
		fprintf(lvl7, "%d\n", i);
	}
	fprintf(lvl7, "quit\n");
	pclose(lvl7);
	return (0);
}
