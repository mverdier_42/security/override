#include <stdio.h>
#include <string.h>

int		main()
{
	char	str[100];
	int		i;

	fgets(str, 100, stdin);
	i = 0;
	while (i < strlen(str)) {
		// str to lower
		if (*(str + i) >= 'A' && *(str + i) <= 'Z')
			*(str + i) = *(str + i) + 32;
		i++;
	}
	printf(str);
	return (exit(0));
}
