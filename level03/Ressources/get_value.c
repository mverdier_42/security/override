#include <stdio.h>
#include <string.h>

int		test(char *key, int pass)
{
	char	str[17];

	for (int i = 0; i < 16; i++) {
		*(str + i) = pass ^ *(key + i);
	}
	*(str + 16) = 0;
	if (strcmp(str, "Congratulations!") == 0)
		return (1);
	return (0);
}

int		main()
{
	for (int i = 1; i < 22; i++) {
		if (test("Q}|u`sfg~sf{}|a3", i)) {
			printf("Value = %d\n", i);
			break ;
		}
	}
	return (0);
}
