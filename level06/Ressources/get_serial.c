#include <stdio.h>
#include <string.h>
#include <stdint.h>

int		main()
{
	char			login[32];
	int				len;
	unsigned int	key;
	int				i;
	long long int	ecx;
	int				tmp;

	printf("-> Enter Login: ");
	fgets(login, 32, stdin);
	*(login + strcspn(login, "\n")) = 0;
	len = strlen(login);
	if (len < 6)
		return (1);
	key = ((*(login + 3) ^ 0x1337) + 0x5eeded);
	i = 0;
	while (i < len) {
		if (*(login + i) < 32)
			return (1);
		ecx = *(login + i) ^ key;
		tmp = (ecx * 0x88233b2b) >> 32;
		// '>>' does not have priority on '+' or '-'
		key = key + (ecx - ((ecx - tmp >> 1) + tmp >> 10) * 1337);
		i++;
	}
	printf("-> Serial is : %u\n", key);
	return (0);
}
